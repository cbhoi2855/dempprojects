package demo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import utils.PropertiesUtils;

import java.io.IOException;
import java.util.Properties;

public class BaseClass {

    WebDriver driver;
    Properties properties;
    String baseURL;
    @BeforeClass
    public void beforeClass() throws IOException {
        String propFileName = System.getProperty("user.dir")+"/src/main/resources/config.properties";
        properties = PropertiesUtils.readPropertiesFile(propFileName);
        baseURL = properties.getProperty("baseURL");
        String basePath = System.getProperty("user.dir");
        String driverPath = basePath+"/src/main/resources/drivers/window/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver",driverPath);
        driver = new ChromeDriver();
    }

    @AfterClass
    public void afterClass(){
        driver.quit();
    }
}
