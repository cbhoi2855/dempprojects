package demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class TestClass extends BaseClass{

    @Test
    public void Activity_1(){
        String searchFromCity = "Ahmedabad";
        String searchToCity = "Bengaluru";
        driver.get("https://www.makemytrip.com/");
//        if(driver.findElement(By.xpath("//*[contains(@class,'loginModal displayBlock modalLogin dynHeight personal')]")).isDisplayed()) {
  //          driver.findElement(By.xpath("//p[text()='Login or Create Account']")).click();
    //    }
        driver.findElement(By.xpath("//a[text()='Search']")).click();
        driver.findElement(By.id("fromCity")).click();
        driver.findElement(By.xpath("//input[@placeholder='Enter City']")).sendKeys(searchFromCity);
        String serchFromXpath = "//span[contains(text(),'"+searchFromCity+"')]";
        driver.findElement(By.xpath(serchFromXpath)).click();

        driver.findElement(By.xpath("//input[@placeholder='Enter City']")).sendKeys(searchToCity);
        String serchToXpath = "//span[contains(text(),'"+searchToCity+"')]";
        driver.findElement(By.xpath(serchToXpath)).click();

        driver.findElement(By.xpath("//div[contains(@class,'--selected')]//following::div[2]")).click();
        driver.findElement(By.id("search-button")).click();

        List<WebElement> listPrice = driver.findElements(By.xpath("//div[@class='priceSection']"));
        String price = listPrice.get(0).findElement(By.xpath("//p")).getText();

        Assert.assertEquals(price,"4000", "Mismatch the price of the airlines");


    }
}
